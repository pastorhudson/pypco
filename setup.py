import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pypco",
    version="1.01",
    author="Bill Detrick",
    description="A python library for accessing Planning Center Online API",
    long_description=long_description,
    long_description_content_type="A python library for accessing Planning Center Online API",
    url="https://github.com/pastorhudson/pypco",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
