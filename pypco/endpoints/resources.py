"""Endpoints for PCO Resources.

To add additional endpoints, simply add additional classes
subclassing the ResourcesEndpoint class.
"""

# pylint: disable=C0304,R0903,C0111,C0321


from .base_endpoint import BaseEndpoint


# The the Resources endpoint
class ResourcesEndpoint(BaseEndpoint): pass # noqa: E701,E302

# All objects on the Resources endpoint
class Attachments(ResourcesEndpoint): pass # noqa: E701,E302
class Conflicts(ResourcesEndpoint): pass # noqa: E701,E302
class Events(ResourcesEndpoint): pass # noqa: E701,E302
class EventInstances(ResourcesEndpoint): pass # noqa: E701,E302
class EventResourceRequests(ResourcesEndpoint): pass # noqa: E701,E302
class EventTimes(ResourcesEndpoint): pass # noqa: E701,E302
class Organizations(ResourcesEndpoint): pass # noqa: E701,E302
class People(ResourcesEndpoint): pass # noqa: E701,E302
class Resources(ResourcesEndpoint): pass # noqa: E701,E302
class ResourceApprovalGroups(ResourcesEndpoint): pass # noqa: E701,E302
class ResourceBookings(ResourcesEndpoint): pass # noqa: E701,E302
class ResourceFolders(ResourcesEndpoint): pass # noqa: E701,E302
class ResourceQuestions(ResourcesEndpoint): pass # noqa: E701,E302
class RoomSetups(ResourcesEndpoint): pass # noqa: E701,E302

