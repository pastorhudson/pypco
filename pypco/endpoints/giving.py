"""Endpoints for PCO Giving.

To add additional endpoints, simply add additional classes
subclassing the GivingEndpoint class.
"""

# pylint: disable=C0304,R0903,C0111,C0321

from .base_endpoint import BaseEndpoint

# The the Giving endpoint
class GivingEndpoint(BaseEndpoint): pass# noqa: E701,E302

# All objects on the Giving endpoint
class Batches(GivingEndpoint): pass# noqa: E701,E302
class BatchGroups(GivingEndpoint): pass# noqa: E701,E302
class Designations(GivingEndpoint): pass# noqa: E701,E302
class DesignationRefunds(GivingEndpoint): pass# noqa: E701,E302
class Donations(GivingEndpoint): pass# noqa: E701,E302
class Funds(GivingEndpoint): pass# noqa: E701,E302
class Labels(GivingEndpoint): pass# noqa: E701,E302
class Organizations(GivingEndpoint): pass# noqa: E701,E302
class PaymentMethods(GivingEndpoint): pass# noqa: E701,E302
class PaymentSources(GivingEndpoint): pass# noqa: E701,E302
class People(GivingEndpoint): pass# noqa: E701,E302
class RecurringDonations(GivingEndpoint): pass# noqa: E701,E302
class RecurringDonationDesignations(GivingEndpoint): pass# noqa: E701,E302
class Refund(GivingEndpoint): pass# noqa: E701,E302

