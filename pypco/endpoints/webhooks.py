"""Endpoints for PCO Webhooks.

To add additional endpoints, simply add additional classes
subclassing the WebhooksEndpoint class.
"""

# pylint: disable=C0304,R0903,C0111,C0321

from .base_endpoint import BaseEndpoint


# The the Webhooks endpoint
class WebhooksEndpoint(BaseEndpoint): pass # noqa: E701,E302

# All objects on the Webhooks endpoint
class AvailableEvents(WebhooksEndpoint): pass # noqa: E701,E302
class Deliveries(WebhooksEndpoint): pass # noqa: E701,E302
class Events(WebhooksEndpoint): pass # noqa: E701,E302
class Organizations(WebhooksEndpoint): pass # noqa: E701,E302
class Subscriptions(WebhooksEndpoint): pass # noqa: E701,E302
