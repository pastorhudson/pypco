"""Endpoints for PCO people.

To add additional endpoints, simply add additional classes
subclassing the PeopleEndpoint class.
"""


# pylint: disable=C0304,R0903,C0111,C0321


from .base_endpoint import BaseEndpoint


# The the people endpoint
class PeopleEndpoint(BaseEndpoint): pass# noqa: E701,E302


# All objects on the people endpoint
class Addresses(PeopleEndpoint): pass # noqa: E701,E302
class Apps(PeopleEndpoint): pass # noqa: E701,E302
class Campuses(PeopleEndpoint): pass # noqa: E701,E302
class Carriers(PeopleEndpoint): pass # noqa: E701,E302
class Emails(PeopleEndpoint): pass # noqa: E701,E302
class FieldData(PeopleEndpoint): pass # noqa: E701,E302
class FieldDefinitions(PeopleEndpoint): pass # noqa: E701,E302
class Households(PeopleEndpoint): pass # noqa: E701,E302
class InactiveReasons(PeopleEndpoint): pass # noqa: E701,E302
class ListCategories(PeopleEndpoint): pass # noqa: E701,E302
class Lists(PeopleEndpoint): pass # noqa: E701,E302
class MaritalStatuses(PeopleEndpoint): pass # noqa: E701,E302
class MessageGroups(PeopleEndpoint): pass # noqa: E701,E302
class Messages(PeopleEndpoint): pass # noqa: E701,E302
class NamePrefixes(PeopleEndpoint): pass # noqa: E701,E302
class NameSuffixes(PeopleEndpoint): pass # noqa: E701,E302
class Notes(PeopleEndpoint): pass # noqa: E701,E302
class People(PeopleEndpoint): pass # noqa: E701,E302
class PeopleImports(PeopleEndpoint): pass # noqa: E701,E302
class PersonMergers(PeopleEndpoint): pass # noqa: E701,E302
class Reports(PeopleEndpoint): pass # noqa: E701,E302
class SchoolOptions(PeopleEndpoint): pass # noqa: E701,E302
class SocialProfiles(PeopleEndpoint): pass # noqa: E701,E302
class Stats(PeopleEndpoint): pass # noqa: E701,E302
class Tabs(PeopleEndpoint): pass # noqa: E701,E302
class Workflows(PeopleEndpoint): pass # noqa: E701,E302
