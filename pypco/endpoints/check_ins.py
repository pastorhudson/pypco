"""Endpoints for PCO check_ins.

To add additional endpoints, simply add additional classes
subclassing the Check_insEndpoint class.
"""


# pylint: disable=C0304,R0903,C0111,C0321


from .base_endpoint import BaseEndpoint


# The the check_ins endpoint
class Check_insEndpoint(BaseEndpoint): pass # noqa: E701,E302


#  All objects on the check_ins endpoint # noqa: E701,E302
class AttendanceTypes(Check_insEndpoint): pass # noqa: E701,E302
class CheckIns(Check_insEndpoint): pass # noqa: E701,E302
class CheckInGroups(Check_insEndpoint): pass # noqa: E701,E302
class Events(Check_insEndpoint): pass # noqa: E701,E302
class EventLabels(Check_insEndpoint): pass # noqa: E701,E302
class EventPeriods(Check_insEndpoint): pass # noqa: E701,E302
class EventTimes(Check_insEndpoint): pass # noqa: E701,E302
class Headcounts(Check_insEndpoint): pass # noqa: E701,E302
class Labels(Check_insEndpoint): pass # noqa: E701,E302
class Locations(Check_insEndpoint): pass # noqa: E701,E302
class LocationEventPeriods(Check_insEndpoint): pass # noqa: E701,E302
class LocationEventTimes(Check_insEndpoint): pass # noqa: E701,E302
class LocationLabels(Check_insEndpoint): pass # noqa: E701,E302
class Options(Check_insEndpoint): pass # noqa: E701,E302
class Organizations(Check_insEndpoint): pass # noqa: E701,E302
class Passes(Check_insEndpoint): pass # noqa: E701,E302
class People(Check_insEndpoint): pass # noqa: E701,E302
class PersonEvents(Check_insEndpoint): pass # noqa: E701,E302
class Stations(Check_insEndpoint): pass # noqa: E701,E302
class Themes(Check_insEndpoint): pass # noqa: E701,E302
