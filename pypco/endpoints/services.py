"""Endpoints for PCO services.

To add additional endpoints, simply add additional classes
subclassing the ServicesEndpoint class.
"""

# pylint: disable=C0304,R0903,C0111,C0321

from .base_endpoint import BaseEndpoint


# The the services endpoint
class ServicesEndpoint(BaseEndpoint): pass # noqa: E701,E302


# All objects on the services endpoint
class Arrangements(ServicesEndpoint): pass # noqa: E701,E302
class Attachments(ServicesEndpoint): pass # noqa: E701,E302
class AttachmentActivities(ServicesEndpoint): pass # noqa: E701,E302
class AttachmentTypes(ServicesEndpoint): pass # noqa: E701,E302
class AvailableSignups(ServicesEndpoint): pass # noqa: E701,E302
class BackgroundChecks(ServicesEndpoint): pass # noqa: E701,E302
class Blockouts(ServicesEndpoint): pass # noqa: E701,E302
class BlockoutDates(ServicesEndpoint): pass # noqa: E701,E302
class BlockoutExceptions(ServicesEndpoint): pass # noqa: E701,E302
class CheckIns(ServicesEndpoint): pass # noqa: E701,E302
class Contributors(ServicesEndpoint): pass # noqa: E701,E302
class EmailTemplates(ServicesEndpoint): pass # noqa: E701,E302
class EmailTemplateRenderedResponses(ServicesEndpoint): pass # noqa: E701,E302
class Folders(ServicesEndpoint): pass # noqa: E701,E302
class Items(ServicesEndpoint): pass # noqa: E701,E302
class ItemNotes(ServicesEndpoint): pass # noqa: E701,E302
class ItemNoteCategories(ServicesEndpoint): pass # noqa: E701,E302
class ItemTimes(ServicesEndpoint): pass # noqa: E701,E302
class Keys(ServicesEndpoint): pass # noqa: E701,E302
class Layouts(ServicesEndpoint): pass # noqa: E701,E302
class Medias(ServicesEndpoint): pass # noqa: E701,E302
class MediaSchedules(ServicesEndpoint): pass # noqa: E701,E302
class NeededPositions(ServicesEndpoint): pass # noqa: E701,E302
class Organizations(ServicesEndpoint): pass # noqa: E701,E302
class People(ServicesEndpoint): pass # noqa: E701,E302
class PersonTeamPositionAssignments(ServicesEndpoint): pass # noqa: E701,E302
class Plans(ServicesEndpoint): pass # noqa: E701,E302
class PlanNotes(ServicesEndpoint): pass # noqa: E701,E302
class PlanNoteCategories(ServicesEndpoint): pass # noqa: E701,E302
class PlanPeople(ServicesEndpoint): pass # noqa: E701,E302
class PlanPersonTimes(ServicesEndpoint): pass # noqa: E701,E302
class PlanTemplates(ServicesEndpoint): pass # noqa: E701,E302
class PlanTimes(ServicesEndpoint): pass # noqa: E701,E302
class Schedules(ServicesEndpoint): pass # noqa: E701,E302
class ScheduledPeople(ServicesEndpoint): pass # noqa: E701,E302
class Series(ServicesEndpoint): pass # noqa: E701,E302
class ServiceTypes(ServicesEndpoint): pass # noqa: E701,E302
class SignupSheets(ServicesEndpoint): pass # noqa: E701,E302
class SignupSheetMetadata(ServicesEndpoint): pass # noqa: E701,E302
class SkippedAttachments(ServicesEndpoint): pass # noqa: E701,E302
class Songs(ServicesEndpoint): pass # noqa: E701,E302
class SongSchedules(ServicesEndpoint): pass # noqa: E701,E302
class SplitTeamRehearsalAssignments(ServicesEndpoint): pass # noqa: E701,E302
class Tags(ServicesEndpoint): pass # noqa: E701,E302
class TagGroups(ServicesEndpoint): pass # noqa: E701,E302
class Teams(ServicesEndpoint): pass # noqa: E701,E302
class TeamLeaders(ServicesEndpoint): pass # noqa: E701,E302
class TeamPositions(ServicesEndpoint): pass # noqa: E701,E302
class TextSettings(ServicesEndpoint): pass # noqa: E701,E302
class TimePreferenceOptions(ServicesEndpoint): pass # noqa: E701,E302
class Zooms(ServicesEndpoint): pass # noqa: E701,E302
